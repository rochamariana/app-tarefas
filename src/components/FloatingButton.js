import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';
import { AntDesign } from '@expo/vector-icons';

const FloatingButton = ({icon, onPress, color}) => {
  
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.button, styles.shadow, {backgroundColor: color ? color : '#bebebe'}]}
    >
      <AntDesign name={ icon ? icon : "plus"} size={32} color="#fff"/>
    </TouchableOpacity>
  )

}

const styles = StyleSheet.create({
  button: {
    borderRadius: 50,
    width: 64,
    height: 64,
    alignItems: 'center',
    justifyContent: 'center',
    position: 'absolute',
    bottom: 20,
    right: 40
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 4
    },
    shadowOpacity: 0.8,
    elevation: 5
  }
});

export default FloatingButton;
