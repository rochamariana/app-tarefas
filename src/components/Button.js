import React from 'react';
import { StyleSheet, Text, TouchableOpacity } from 'react-native';

const Button = ({label, onPress}) => {
  
  return (
    <TouchableOpacity
      onPress={onPress}
      style={[styles.button, styles.shadow]}
    >
      <Text style={styles.textButton}>{label}</Text>
    </TouchableOpacity>
  )

}

const styles = StyleSheet.create({
  button: {
    backgroundColor: '#fff',
    borderRadius: 12,
    marginTop: 30,
    padding: 10,
    justifyContent: 'center',
    alignItems: 'center',
    width: "80%"
  },
  textButton: {
    fontSize: 18,
    fontFamily: "OpenSans",
    fontWeight: 'bold',
    textAlign: 'center',

  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 4
    },
    shadowOpacity: 0.8,
    elevation: 15
  }
});

export default Button;
