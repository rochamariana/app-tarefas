import React, { createContext, FunctionComponent, ReactElement, useState } from 'react';

interface Props {
  children: ReactElement
}

export interface IAppContext {
  username: string;
  senha: string;
  saveUser: (username: string, senha: string) => void;
}

//Criação do contexto com um tipo de informação
export const AppContext = createContext({} as IAppContext);

export const AppProvider: FunctionComponent<Props> = ({ children }) => {

  const [usuario, setUsuario] = useState({
    username: '',
    senha: ''
  });

  function saveUser (username: string, senha: string) {
    setUsuario({
      username: username,
      senha: senha
    });
  };

  return (
    <AppContext.Provider value={{
      username: usuario.username,
      senha: usuario.senha,
      saveUser: saveUser
    } as IAppContext}>
      {children}
    </AppContext.Provider>
  );

}