import React, { useState, useEffect } from "react";
import { ActivityIndicator, Alert, KeyboardAvoidingView, Platform, StyleSheet, TextInput, View, Text } from "react-native";
import CustomButton from '../components/Button'
import * as SecureStore from 'expo-secure-store';
import Checkbox from 'expo-checkbox';

const base64 = require('base-64');


const ViewLogin = ({ navigation }) => {

  const [loading, setLoading] = useState(false)
  const [usuario, setUsuario] = useState({
    username: '',
    senha: '',
    saveUser: false,
  });

  // setTimeout(() => {

  // })

  useEffect(() => {

    async function getSecureStore() {

      const _username = await SecureStore.getItemAsync('myapp_user');
      const _password = await SecureStore.getItemAsync('myapp_password');

      if (_username && _password) {

        /*setUsuario({
            username: _username,
            password: _password,
            saveUser: true
        });*/
        login(_username, _password);
      }
    }

    getSecureStore();

  }, []) //seja executado somente na primeira renderizacao do componente


  function login(user, pass) {

    setTimeout(() => {

      async function testLogin() {
        const res = await fetch('http://177.44.248.30:3333/auth', {
          method: 'POST',
          headers: {
            'Authorization': 'Basic ' + base64.encode(user + ':' + pass)
          }
        });

        const json = await res.json();
        console.log(json)

        if (json.id) {
          if (usuario.saveUser) {
            await SecureStore.setItemAsync('myapp_user', usuario.username)
            await SecureStore.setItemAsync('myapp_password', usuario.senha)
            console.log('login gravado')
          } else {
            await SecureStore.deleteItemAsync('myapp_user')
            await SecureStore.deleteItemAsync('myapp_password')
          }
          
          navigation.navigate('ViewList')

        } else {
          Alert.alert('Opa, algo deu errado!', "Email ou senha incorretos")
        }

      }

      testLogin()

    }, 900)
  }

  // async function login() {
  //   const response = await fetch('http://177.44.248.62:3001/tasks');
  //   const json = await response.json();
  //   console.log('res=>', json)
  // }

  return (

    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>

      {loading ? <ActivityIndicator size={60} /> :
        <>
          <TextInput
            keyboardType="email-address"
            autoCapitalize="none"
            style={styles.input}
            value={usuario.username}
            onChangeText={(value) => setUsuario({ ...usuario, username: value })}
            placeholder='Usuário'
            placeholderTextColor='#fff'
            />

          <TextInput
            autoCapitalize="none"
            secureTextEntry={true} //senha 'escondida'
            style={styles.input}
            value={usuario.senha}
            onChangeText={(value) => setUsuario({ ...usuario, senha: value })}
            placeholder='Senha' 
            placeholderTextColor='#fff'/>

          <View style={styles.checkbox}>
            <Checkbox
              value={usuario.saveUser}
              color={usuario.saveUser ? '#bebebe' : '#fff'}
              onValueChange={() =>
                setUsuario({ ...usuario, saveUser: !usuario.saveUser })
              }

            />

            <Text style={styles.text}>Mantenha-me conectado</Text>
          </View>

          <CustomButton
            label="ENTRAR"
            onPress={() => login(usuario.username, usuario.senha)} />
        </>
      }

    </KeyboardAvoidingView>

  )
}

export default ViewLogin;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#6A5ACD',

  },
  input: {
    height: 40,
    width: '80%',
    color: "#fff",
    borderWidth: 1,
    borderColor: "#fff",
    borderRadius: 10,
    marginBottom: 20,
    paddingLeft: 8
  },
  text: {
    color: '#fff',
    fontSize: 14,
    marginLeft: 8
  },
  checkbox: {
    width: '80%',
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 10
  }
})