import React, { useState, useEffect, useContext } from "react";
import LoginScreen from "react-native-login-screen";
import { ActivityIndicator, Alert, KeyboardAvoidingView, Platform, StyleSheet } from "react-native";
import { AppContext } from '../context/AppContext';
import { ForceTouchGestureHandler } from "react-native-gesture-handler";

const base64 = require('base-64');

import axios from "axios";

const ViewNewLogin = ({ navigation }) => {
  const [loading, setLoading] = useState(false)
  
  const novoUsuario = {
    username: '',
    senha: '',
  };

  const { username, senha, saveUser } = useContext(AppContext)
  console.log('credentials ', username, senha)
 
  function login(user, pass) {

      console.log('login', user, pass)

      async function testLogin() {
        try {
            const AUTH_TOKEN = 'Basic ' + base64.encode(user + ":" + pass);
            const response = await axios.get('/auth', {
                headers: {
                    'Authorization': AUTH_TOKEN
                }
            })

            if (response.status === 200) {
                axios.defaults.headers.common['Authorization'] = AUTH_TOKEN
                // saveUser(user, pass);

                const json = response.data;
                console.log('RETORNOU JSON', json);
                
                navigation.reset({
                    index: 0,
                    routes: [{ name: "ViewList" }]
                })

            } else if (response.status === 400) {
                Alert.alert('Que pena 😥', json.message);
            } else if (response.status === 401) {
                Alert.alert('Tente novamente 😥', json.error);
            }

            
        } catch (error) {

            Alert.alert('Algo está errado', error.message);

        } finally {
            setLoading(false);
        }
    }
      
      testLogin()

  }

  return (

    <KeyboardAvoidingView
      behavior={Platform.OS === 'ios' ? 'padding' : 'height'}
      style={styles.container}>

      {loading === true ? <ActivityIndicator size={60} /> :
        <LoginScreen
          logoImageSource={require("../../assets/logo-agency.png")}       

          emailPlaceholder='Usuário'
          onEmailChange={(username) => { novoUsuario.username = username }}

          passwordPlaceholder="Senha"
          onPasswordChange={(senha) => { novoUsuario.senha = senha }}
         
          onLoginPress={() => login(novoUsuario.username, novoUsuario.senha)}
          loginButtonStyle={{backgroundColor: 'purple'}}

          signupText="Criar nova conta"
          onSignupPress={() => { }}

          disableSocialButtons={true}

        />
      }
    </KeyboardAvoidingView>

  )

}

export default ViewNewLogin;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'

  }
})