import React, { useState, useEffect, useContext, useRef } from 'react';
import { ScrollView, Text, TextInput, View, StyleSheet, FlatList, TouchableOpacity, KeyboardAvoidingView, SafeAreaView } from 'react-native';

// componentes
import FloatingButton from '../components/FloatingButton';
import CustomButton from '../components/Button';

// contexto
import { AppContext } from '../context/AppContext';

import SkeletonPlaceholder from 'react-native-skeleton-placeholder';
import { LinearGradient } from 'expo-linear-gradient';
import { Modalize } from 'react-native-modalize';

const base64 = require('base-64');

import { AntDesign } from '@expo/vector-icons';
import axios from 'axios';

const ViewList = () => {

  const initialUser = {
    id: 0,
    name: '',
    email: '',
    password: '',
    age: 0,
    sex: ''
  }

  const fieldUser = "myapp_user";
  const fieldPassword = "myapp_password";
  const [loading, setLoading] = useState(false);
  const [users, setUsers] = useState([]);
  const [user, setUser] = useState(initialUser);

  const { username, password } = useContext(AppContext);
  console.log('lista users ', username, password);

  const modalRef = useRef(null);

  function onOpen() {
    if (modalRef != null) {
      modalRef.current?.open();
    }
  };

  function alterUser(user) {
    onOpen()
    setUser(user)
  }

  function newUser() {
    onOpen()
    setUser(initialUser)
  }

  async function salvarUser() {

    try {

      if (user.name == '') {
        Alert.alert('Digite o nome completo')
        return;
      }

      if (user.email == '') {
        Alert.alert('Informe um e-mail');
        return;
      }
      
      if (user.age <= 0) {
        Alert.alert('Informe a idade');
        return;
      }

      const payload = {
        age: user.age,
        email: user.email,
        name: user.name,
        password: user.password,
        sex: user.sex
      }

      const response = null;

      if (user.id > 0) {

        response = await axios.put(`/users/${user.id}`, payload);
      } else {

        response = await axios.post(`/users/`, payload);
      }

      if (response && response.status === 200) {
        modalRef.current?.close();
      }

    } catch (error) {

      Alert.alert('Algo de errado não está certo!')

    }
  }

  /*Busca os usuários da API (através do listUsers)
      na criação do componente ViewUsers*/
  useEffect(() => {
    listUsers();
  }, [])

  async function listUsers() {

    setLoading(true);

    const response = await axios.get('/users')
    const json = response.data;

    if (response.status === 200) {
      setUsers(json);
    } else {
      Alert.alert('Ops, deu ruim 😥', json.message);
    }
    setLoading(false);
  }

  return (
      <View style={styles.container}>
        <Text style={styles.text}>LISTA DE USUÁRIOS</Text>

        <FlatList
          data={users}
          onRefresh={() => listUsers()}
          refreshing={loading}
          keyExtractor={(item) => item.id}

          renderItem={({ item }) => {

            const icone_sexo = item.sex == 'M' ? 'man' : 'woman';

            return <TouchableOpacity
              activeOpacity={0.6}
              style={[styles.card, styles.shadow]}
              key={item.id}
              onPress={() => alterUser(user)}
            >

              <View style={{ flexDirection: 'row', alignItems: 'center' }}>

                <AntDesign
                  name={icone_sexo}
                  size={24}
                  color={item.sex == 'M' ? "#7986CB" : "#F06292"}
                  style={{ marginRight: 16 }} />

                <View>
                  <Text style={styles.titleCard}>{item.name}</Text>
                  <Text style={styles.subtitleCard}>{item.email}</Text>
                </View>

              </View>
              <AntDesign
                name="right"
                size={24}
                color="black" />
            </TouchableOpacity>
          }
          }
        />

        <FloatingButton icon='plus' color="#6A5ACD" onPress={() => newUser()} />


        <Modalize
          ref={modalRef}
          snapPoint={200}
          modalHeight={300}
        >

          <KeyboardAvoidingView>
            <View style={[styles.modal]}>
              <Text style={styles.modalTitle}>ALTERAR USUÁRIO</Text>
              <TextInput
                keyboardType='email-address'
                autoCapitalize='words'
                value={user.name}
                onChangeText={(name) => setUser({ ...user, name: name })}
                style={styles.input}
                placeholder='Nome'
                placeholderTextColor='#bebebe'
              />
              <TextInput
                keyboardType='number-pad'
                autoCapitalize='none'
                value={user.age.toString()}
                onChangeText={(age) => setUser({ ...user, age: age })}
                style={styles.input}
                placeholder='Idade'
                placeholderTextColor='#bebebe'
              />
              <TextInput
                keyboardType='email-address'
                autoCapitalize='none'
                value={user.email}
                onChangeText={(email) => setUser({ ...user, email: email })}
                style={styles.input}
                placeholder='E-mail'
                placeholderTextColor='#bebebe'
              />
              <TextInput
                keyboardType='email-address'
                autoCapitalize='none'
                value={user.password}
                onChangeText={(password) => setUser({ ...user, password: password })}
                style={styles.input}
                placeholder='Senha'
                placeholderTextColor='#bebebe'
              />

              <TouchableOpacity onPress={(salvarUser)}>
                <Text style={styles.textList}>SALVAR</Text>
              </TouchableOpacity>

            </View>

          </KeyboardAvoidingView>

        </Modalize>

        {/* <ScrollView>
        <View style={{ flex: 1 }}>
          {
            users.map((item) => {
              console.log('ITEM=>', item)
              return (
                <View key={item.id}>
                  <Text style={styles.textList}>{item.name}</Text>
                </View>
              )
            })
          }
        </View>
      </ScrollView> */}

      </View>

  );
};

export default ViewList;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    backgroundColor: '#fff',
    paddingTop: 30,
  },
  text: {
    fontSize: 30,
    color: '#6A5ACD',
    fontFamily: "Amaranth_400Regular"
  },
  textList: {
    color: '#000',
    fontSize: 16,
    fontFamily: "OpenSans",
    paddingLeft: 10
  },
  card: {
    margin: 10,
    borderRadius: 16,
    padding: 8,
    height: 55,
    backgroundColor: '#f1f1f1',
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  titleCard: {
    color: '#000',
    fontSize: 16,
    fontFamily: 'OpenSans'
  },
  subtitleCard: {
    color: '#555',
    fontSize: 13,
    fontFamily: 'OpenSans',
  },
  shadow: {
    shadowColor: '#000',
    shadowOffset: {
      width: 2,
      height: 3
    },
    shadowOpacity: 0.8,
    elevation: 5
  },
  modal: {
    flex: 1
  },
  input: {
    height: 40,
    color: "#000",
    borderWidth: 1,
    borderColor: "#000",
    borderRadius: 10,
    margin: 8,
    paddingLeft: 10
  },
  modalTitle: {
    color: '#000',
    fontSize: 16,
    fontFamily: 'OpenSans',
    paddingLeft: 12,
    marginTop: 10
  }

})