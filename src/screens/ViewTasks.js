import React, { useEffect, useState } from "react";
import { Dimensions, StyleSheet, TextInput, Text, View, Alert, TouchableOpacity } from 'react-native';
import CustomButton from '../components/Button';
import LottieView from 'lottie-react-native';
import Checkbox from "expo-checkbox";
import { Ionicons } from '@expo/vector-icons';
import AsyncStorage from '@react-native-async-storage/async-storage';


const { width } = Dimensions.get('window');

const ViewTasks = () => {

  const [taskList, setTaskList] = useState([]);
  const [task, setTask] = useState('');

  useEffect(() => {
    const getTasksAsync = async () => {
      try {
        const variavel = await AsyncStorage.getItem('@tasklist');
        if (variavel != null) {
          setTaskList(JSON.parse(variavel));
        }
      } catch (e) {
        console.log('ERROR GET=>', e)
      }
    }
    getTasksAsync()
  }, [])

  useEffect(() => {
    const setTaskAsync = async () => {
      try {
        if (taskList) {
          await AsyncStorage.setItem('@tasklist', JSON.stringify(taskList));
        }
      } catch (e) {
        console.log('ERROR=>', e);
      }
    }
    setTaskAsync()
  }, [taskList])

  const deleteTask = (id) => {
    Alert.alert("Atenção", 'Voce gostaria de deletar essa tarefa?', [{
      text: "Deletar",
      onPress: async () =>
        setTaskList([...taskList.filter((item) => item.id !== id)]),
    }, {
      text: "Não Deletar",
      onPress: () => { }
    }])
  }

  const updateList = () => {

    if (task) {
      const newTask = {
        id: String(new Date().getTime()),
        name: task,
        done: false
    }

      const orderTaskList = [...taskList, newTask].sort((a, b) => {

        if (a.name > b.name) {
          return 1;
        } else if (b.name > a.name) {
          return -1;
        } else {
          return 0;
        }

      })

      // .sort((a, b) => (a.name > b.name ? 1 : (b.name > a.name ? -1 : 0)) -> mesmo if acima, mas simplificado

      setTaskList(orderTaskList)
      setTask('');

      // await AsyncStorage.setItem('@taskList', JSON.stringify(orderTaskList));

    } else {
      Alert.alert('Opa!', 'Não tem como cadastrar tarefa em branco, colega')
    }
  }

  const handleCheckTask = (id) => {
    const newTaskList = taskList.map(item => {
      if (item.id === id) {
        return { ...item, done: !item.done }
      }
      return item;
    })
    setTaskList(newTaskList);
  }

  return (

    <View style={styles.container}>
      <TextInput
        style={styles.input}
        keyboardType="default"
        placeholder="Digite a tarefa"
        placeholderTextColor='#fff'
        value={task}
        onChangeText={(value) => { setTask(value) }}
      />

      <CustomButton onPress={updateList}
        label={'ADICIONAR'}
      />
      {
        taskList != null && taskList.length > 0 ?
          taskList.map((item) => {
            return (
              <View
                key={item.id}
                style={styles.itemList}>
                <Checkbox
                  value={item.done}
                  onValueChange={() => handleCheckTask(item.id)}
                  color={item.done ? '#444' : '#fff'}
                />
                <Text style={[styles.itemText, { textDecorationLine: item.done ? 'line-through' : 'none', color: item.done ? '#bebebe' : '#fff', }]}>{item.name}</Text>
                <Ionicons name="trash-outline" size={24} color="#fff" onPress={() => deleteTask(item.id)} />
              </View>
            )
          })
          :
          <View style={{ flex: 1, width: '100%', justifyContent: 'center', alignItems: 'center' }}>
            <Text style={styles.text}>LISTA VAZIA</Text>
            <LottieView
              autoPlay
              style={{
                width: 200,
                height: 200,
                backgroundColor: '#6A5ACD',
              }}
              source={require('../../assets/animations/empty-astronaut.json')}
            />

          </View>
      }

    </View >

  );
};

export default ViewTasks;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#6A5ACD',
    alignItems: 'center',
    height: 30,
  },
  input: {
    paddingTop: 20,
    borderBottomColor: '#fff',
    borderBottomWidth: 1,
    width: (width * 0.8),
    color: '#fff'
  },
  text: {
    color: '#fff',
    marginTop: 20,
    fontWeight: 'bold',
    fontSize: 24
  },
  itemList: {
    width: '90%',
    marginTop: 20,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  itemText: {
    flex: 1,
    color: '#fff',
    fontWeight: 'bold',
    fontSize: 18,
    marginLeft: 10
  }
});