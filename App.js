import { StatusBar } from 'expo-status-bar';

import ViewTasks from './src/screens/ViewTasks';
import ViewLogin from './src/screens/ViewLogin';
import ViewList from './src/screens/ViewList';

import { NavigationContainer } from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack';
import { ActivityIndicator } from 'react-native';

import { useFonts } from 'expo-font';
import { Aladin_400Regular } from '@expo-google-fonts/aladin'
import { Amaranth_400Regular, Amaranth_700Bold, Amaranth_400Regular_Italic } from '@expo-google-fonts/amaranth'
import { AppContext, IAppContext } from './src/context/AppContext';
import ViewNewLogin from './src/screens/ViewNewLogin';

import axios from 'axios';
import config from './src/config/config'

const Stack = createNativeStackNavigator();


export default function App() {

  axios.defaults.baseURL = config.baseURL;

  const [fontsLoaded] = useFonts({
    "OpenSans": require('./assets/fonts/OpenSans/OpenSans-Regular.ttf'),
    Aladin_400Regular,
    Amaranth_400Regular,
    Amaranth_700Bold,
    Amaranth_400Regular_Italic
  });


  if (fontsLoaded) {
    return (
      <AppContext.Provider value={{
          username: 'mateus@mateus.com',
          password: '12345'
        }}>

        <NavigationContainer>
          <Stack.Navigator screenOptions={{
            headerShown: false, animation: 'fade'
          }}
            initialRouteName='ViewNewLogin'>
            <Stack.Screen name='ViewList' component={ViewList} />
            <Stack.Screen name='ViewTasks' component={ViewTasks} />
            <Stack.Screen name='ViewLogin' component={ViewLogin} />
            <Stack.Screen name='ViewNewLogin' component={ViewNewLogin} />

          </Stack.Navigator>

        </NavigationContainer>


        {/* //   <ViewImages /> */}
        <StatusBar
          translucent={false}
          backgroundColor="#fff"
          style="auto" />
      </ AppContext.Provider>
    );
  } else {
    return (
      <>
        <ActivityIndicator size={30} />
        <StatusBar
          translucent={false}
          backgroundColor="#fff"
          style="auto" />
      </>
    )
  }
}

